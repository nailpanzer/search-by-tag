import os
from contextlib import contextmanager
from typing import Any, Generator

from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session


# create db
SQLALCHEMY_DATABASE_URL = "sqlite:///./my_databese.db"
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


# create tables
class DocumentModel(Base):
    __tablename__ = "documents"

    id: int = Column(Integer, primary_key=True, index=True)
    tags: str = Column(String)
    text: str = Column(String)

class IndexTagModel(Base):
    __tablename__ = "include_tag_index"

    id: int = Column(Integer, primary_key=True, index=True)

    # indexed tags
    tag_name: str = Column(String, index=True)
    tag_value: str = Column(String, index=True)

    # document that contains in column tags "tag_name: tag_value"
    document_id: int = Column(Integer, index=True)


Base.metadata.create_all(bind=engine)


@contextmanager
def get_db() -> Generator[Session, Any, None]:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

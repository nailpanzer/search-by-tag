import json
from typing import Dict, List
from database import DocumentModel, IndexTagModel, get_db
import random

from sqlalchemy.orm import Session


possible_tags = [f"tag{i}" for i in range(10)]
none_value = "*"


#
# Add some data to db
#
def get_tags(current_tags: Dict, possible_tags: List) -> Dict[str, str]:
    """
    Adds tags that are not in the document
    Example:
        current_tags={tag1: "abc"}
        possible_tags=[tag1, tag2]
        returns: {tag1: "abc", tag2: "*"}
    """
    result_tags = current_tags.copy()
    for tag in possible_tags:
        if tag not in result_tags:
            result_tags[tag] = none_value

    return result_tags


def add_some_data(docs_count=50) -> None:
    """Generates random data and adds random tags"""
    with get_db() as db:
        for i in range(docs_count):
            tags_count = random.randint(1, 5)
            random_tags = random.sample(possible_tags, tags_count)
            tags_values = [f"Value_{random.randint(0, 10)}" for _ in range(tags_count)]

            result_tags = get_tags(dict(zip(random_tags, tags_values)), possible_tags)

            doc = DocumentModel(
                tags=json.dumps(result_tags, sort_keys=True),
                text="Some text",
            )

            db.add(doc)
            db.commit()
            db.refresh(doc)

            # add strings to index
            for tag, value in result_tags.items():
                db.add(
                    IndexTagModel(
                        tag_name=tag,
                        tag_value=value,
                        document_id=doc.id,
                    )
                )

            db.commit()


# add_some_data()


#
# Searching with some logic operaions
#
def get_documents_id_from_index(
    db: Session,
    include_tags: List[
        Dict[str, str]
    ],  # [{tag1: value AND tag2: value} or {tag1: value AND tag2: value}]
) -> List[int]:
    docs_ids = []

    # include tags
    for include_and_tags in include_tags:
        docs_and_sets = []
        for key, val in include_and_tags.items():
            index_query = db.query(IndexTagModel)

            if val == none_value:
                # if you ALL tag values are included like this
                index_query = index_query.filter(
                    IndexTagModel.tag_name == key,
                    IndexTagModel.tag_name != none_value,
                )
            else:
                index_query = index_query.filter(
                    IndexTagModel.tag_name == key,
                    IndexTagModel.tag_value == val,
                )
            docs_and_sets.append(
                set([index_o.document_id for index_o in index_query.all()])
            )

        # AND operation
        total_and_docs_ids = []
        if len(docs_and_sets) > 0:
            total_and_docs_ids = docs_and_sets[0].copy()
            for i in range(1, len(docs_and_sets)):
                total_and_docs_ids = total_and_docs_ids.intersection(docs_and_sets[i])

        # OR operation
        docs_ids.extend(total_and_docs_ids)

    return docs_ids


#
def find_docs_by_tags(
    db: Session,
    include_tags: List[
        Dict[str, str]
    ],  # [{tag1: value AND tag2: value} or {tag1: value AND tag2: value}]
    exclude_tags: List[
        Dict[str, str]
    ],  # [{tag1: value AND tag2: value} or {tag1: value AND tag2: value}]
    page_n=0,
    per_page=10,
) -> List[DocumentModel]:
    docs_indexes = []

    included_docs_indexes = get_documents_id_from_index(db, include_tags)
    excluded_docs_indexes = get_documents_id_from_index(db, exclude_tags)

    for i in included_docs_indexes:
        if i not in excluded_docs_indexes:
            docs_indexes.append(i)

    return (
        db.query(DocumentModel)
        .filter(DocumentModel.id.in_(docs_indexes))
        .slice(page_n * per_page, page_n * per_page + per_page)
        .all()
    )


#
# Test
#
find_docs_list = []

with get_db() as db:
    find_docs_list = find_docs_by_tags(
        db,
        include_tags=[
            # "tag0": "Value_7" AND "tag1": "Value_5" OR "tag0": "Value_4"
            {"tag0": "Value_7", "tag1": "Value_5"},
            {"tag0": "Value_4"},
            {"tag1": "Value_6"},
        ],
        exclude_tags=[
            {"tag0": "Value_2"}
        ],
        page_n=1,
        per_page=3,
    )


for i, doc in enumerate(find_docs_list):
    print(f"{i}: {doc.id}, {doc.tags}")
